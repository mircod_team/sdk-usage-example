### About ###

This library based on rxJava 1.x.  

### Setup ###

Add this lines in your app module files 

*build.gradle*

```
#!groovy
repositories { 
    maven {
        url "https://mymavenrepo.com/repo/2rONZ9qAio6LP05rUiBn/"
    }
}

dependencies {
    compile ('com.mircod:sdk:0.2.6') {
        transitive = true
    }
}


```
*AndroidManifest.xml*
```
#!xml

<uses-permission android:name="android.permission.BLUETOOTH"/>
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" /> <!--optional-->
```



### Usage

* Obtain instance of *Client* by calling
```
#!java
Client.instance(context)

```
* Scan for nearby devices. It's requires Location searching to be enabled. (Android BLE specs)
```
#!java
Client.instance(context).scanDevices(filter)  // filter - if true then only mircod devices will be shown in results, else all BLE devices

```
* Select device
```
#!java
Client.instance(context).connect(mac)

```
* Subscribe to the required data
```
#!java
Client.instance(context).getSensorStream(AppSensor)


```
Available *AppSensor* values
```
#!java
ECG              // Filtered ecg data
ECG_LEAD_OFF     // Is all electrodes are attached to body
ECG_RR           // Respiration rate variation obtained from EGG data
ECG_HR           // Respiration rate variation obtained from EGG data
ECG_HRV          // Respiration rate variation obtained from EGG data
GSR              // Galvanic skin response
PRESSURE         // Barometric air pressure
AIR_TEMPERATURE  // Air temperature in C with 1 decimal place
BODY_TEMPERATURE // Air temperature in C with 1 decimal place
YAW              // Yaw. Range (-180, 180)
PITCH            // Pitch. Range (-180, 180)
ROLL             // Roll. Range (-90, 90)
OPTICAL_LEAD_OFF // Status if the optical sensor is attached to the skin
SPO2             // Blood oxygen saturation in %
OPTICAL_HR       // Heart rate (heart beats per min) value obtained from optical sensor
OPTICAL_HRV      // Heart rate variation value obtained from optical sensor


```
Raw data from device sensor can be obtained via
```
#!java
Client.instance(context).getDeviceSensorStream(DeviceSensor)
```
Available *DeviceSensor* values
```
#!java
ECG               
GSR
PRESSURE
AIR_TEMP
ECG_LEAD_OFF
BODY_TEMP
ORIENTATION
OPTICAL
OPTICAL_LEAD_OFF
```

### Misc ###
SDK is capable simulating work with real device. It's controlled by changing value with
```
#!java
Client.setDummyData(true)

```
This should be called before scanning for devices.


### Warning ###
This library does not automaticaly request system permissions on Android M (23) or older and requests to enable bluetooth. It is necessary to implement this functionality before calling sdk methods.





### Contacts 
For any questions, please contact [daniil@mircod.com](mailto:daniil@mircod.com)