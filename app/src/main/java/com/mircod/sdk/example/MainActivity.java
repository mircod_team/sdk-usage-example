package com.mircod.sdk.example;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jakewharton.rxbinding.view.RxView;
import com.mircod.sdk.client.Client;
import com.mircod.sdk.models.AppSensor;
import com.mircod.sdk.models.IDevice;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.PublishSubject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.scan).setOnClickListener(this);
        findViewById(R.id.disconnect).setOnClickListener(this);
        //  Client.setDummyData(true);
        LinearLayout container = (LinearLayout) findViewById(R.id.sensors);
        int padding = (int) (getResources().getDisplayMetrics().density * 4);
        for (final AppSensor s : AppSensor.all) {
            CheckBox ch = new CheckBox(this);
            ch.setText(s.getName());
            ch.setPadding(padding, padding, padding, padding);
            container.addView(ch, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            PublishSubject<Object> stopTrigger = PublishSubject.create();
            ch.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (!isChecked) {
                    stopTrigger.onNext("stop"); // unsubscribe on uncheck
                    return;
                }

                Client.instance(MainActivity.this).getSensorStream(s)
                        .takeUntil(stopTrigger) // listen for uncheck event
                        .takeUntil(RxView.detaches(ch)) // unsubscribe if ui detaches
                        .sample(100, TimeUnit.MILLISECONDS) // Lets not force ui update to frequent
                        .map(value -> String.format(Locale.ENGLISH, "%s: %.2f", s.getName(), value)) // format to human readable
                        .observeOn(AndroidSchedulers.mainThread()) // move to ui thread
                        .subscribe(ch::setText, Throwable::printStackTrace);
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scan: {
                showScanDialog();
                break;
            }
            case R.id.disconnect: {
                disconnect();
                break;
            }
        }
    }

    private void showScanDialog() {
        final DeviceListAdapter adapter = new DeviceListAdapter();
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Scanning...")
                .adapter(adapter, new LinearLayoutManager(this)).show();
        adapter.onDeviceSelectedListener = iDevice -> {
            connect(iDevice.getMac());
            dialog.dismiss();
        };

        Client.instance(this)
                .scanDevices()
                .takeUntil(RxView.detaches(dialog.getView()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(iDevice -> {
                    if (adapter.list.contains(iDevice)) {
                        // Just show updated info on same place
                        adapter.updateDevice(iDevice);
                    } else {
                        // Append new device
                        adapter.newDevice(iDevice);
                    }
                }, Throwable::printStackTrace);

    }

    private void connect(String mac) {
        findViewById(R.id.sensors).setVisibility(View.VISIBLE);
        Client.instance(this)
                .connectToDevice(mac)
                .takeUntil(disconnectTrigger)
                .subscribe(connectionState -> Log.d("State:", connectionState.name()), Throwable::printStackTrace);
    }

    PublishSubject<Object> disconnectTrigger = PublishSubject.create();

    void disconnect() {
        findViewById(R.id.sensors).setVisibility(View.GONE);
        disconnectTrigger.onNext(null);
    }

    private static class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.VH> {
        OnDeviceSelectedListener onDeviceSelectedListener = null;
        ArrayList<IDevice> list = new ArrayList<>();

        @Override
        public VH onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_device, parent, false);
            final VH holder = new VH(v);
            holder.itemView.setOnClickListener(v1 -> {
                if (onDeviceSelectedListener != null) {
                    onDeviceSelectedListener.onDeviceSelected(holder.iDevice);
                }
            });
            return holder;
        }

        @Override
        public void onBindViewHolder(VH holder, int position) {
            holder.setDevice(list.get(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        void newDevice(IDevice iDevice) {
            list.add(iDevice);
            notifyItemInserted(list.size() - 1);
        }

        void updateDevice(IDevice iDevice) {
            int index = list.indexOf(iDevice);
            list.set(index, iDevice);
            notifyItemChanged(index);
        }


        static class VH extends RecyclerView.ViewHolder {
            IDevice iDevice;
            TextView name;
            TextView mac;

            VH(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(android.R.id.text1);
                mac = (TextView) itemView.findViewById(android.R.id.text2);
            }

            void setDevice(IDevice iDevice) {
                this.iDevice = iDevice;
                name.setText(iDevice.getName());
                mac.setText(iDevice.getMac());
            }
        }

        interface OnDeviceSelectedListener {
            void onDeviceSelected(IDevice iDevice);
        }
    }
}
